/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * vanillajax
 * @see {@link https://framagit.org/tla/vanillajax}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/vanillajax/blob/master/LICENSE}
 *
 * @use js-tools-kit
 * @see {@link https://framagit.org/tla/js-tools-kit}
 * */
( function () {
    
    'use strict';
    
    var global ,
        hasProperty = Object.prototype.hasOwnProperty;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    /**
     * js-tools-kit
     * @license MIT
     * @see {@link https://framagit.org/tla/js-tools-kit}
     * */
    ( function () {
        
        function _defineProperty ( obj , key , prop ) {
            Object.defineProperty( obj , key , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : prop
            } );
        }
        
        /**
         * @readonly
         * @property {Window} global
         * */
        _defineProperty( global , 'global' , global );
        
        /**
         * @readonly
         * @function _defineProperty
         * @param {Object} obj
         * @param {string} key
         * @param {*} prop
         * */
        _defineProperty( global , '_defineProperty' , _defineProperty );
        
        /**
         * @readonly
         * @function isset
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isset' , function ( _ ) {
            return _ !== undefined;
        } );
        
        /**
         * @readonly
         * @function isnull
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnull' , function ( _ ) {
            return _ === null;
        } );
        
        /**
         * @readonly
         * @function exist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'exist' , function ( _ ) {
            return isset( _ ) && !isnull( _ );
        } );
        
        /**
         * @readonly
         * @function isdate
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdate' , function ( _ ) {
            return exist( _ ) && _ instanceof Date;
        } );
        
        /**
         * @readonly
         * @function isarray
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isarray' , function ( _ ) {
            return exist( _ ) && Array.isArray( _ );
        } );
        
        /**
         * @readonly
         * @function isobject
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isobject' , function ( _ ) {
            return exist( _ ) &&
                _ instanceof Object && _.toString() === '[object Object]' &&
                _.constructor.hasOwnProperty( 'defineProperty' );
        } );
        
        /**
         * @readonly
         * @function isstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isstring' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'string' || _ instanceof global.String );
        } );
        
        /**
         * @readonly
         * @function isfillstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfillstring' , function ( _ ) {
            return isstring( _ ) && !!_.trim();
        } );
        
        /**
         * @readonly
         * @function isboolean
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isboolean' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'boolean' || _ instanceof global.Boolean );
        } );
        
        /**
         * @readonly
         * @function isinteger
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isinteger' , function ( _ ) {
            return exist( _ ) && Number.isInteger( _ );
        } );
        
        /**
         * @readonly
         * @function isfloat
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfloat' , function ( _ ) {
            return exist( _ ) && typeof _ == 'number' && isFinite( _ );
        } );
        
        /**
         * @readonly
         * @function isfunction
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfunction' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'function' || _ instanceof global.Function );
        } );
        
        /**
         * @readonly
         * @function isevent
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isevent' , function ( _ ) {
            if ( exist( _ ) ) {
                return ( _ instanceof global.Event ) ||
                    ( exist( _.defaultEvent ) && _.defaultEvent instanceof global.Event ) ||
                    ( exist( _.originalEvent ) && _.originalEvent instanceof global.Event );
            }
            return false;
        } );
        
        /**
         * @readonly
         * @function isregexp
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isregexp' , function ( _ ) {
            return exist( _ ) && ( _ instanceof global.RegExp );
        } );
        
        /**
         * @readonly
         * @function isnodelist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnodelist' , function ( _ ) {
            return exist( _ ) && (
                ( _ instanceof global.NodeList || _ instanceof global.HTMLCollection ) ||
                ( !isset( _.slice ) && isset( _.length ) && typeof _ === 'object' )
            );
        } );
        
        /**
         * @readonly
         * @function isdocument
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdocument' , function ( _ ) {
            return exist( _ ) && exist( _.defaultView ) && _ instanceof _.defaultView.HTMLDocument;
        } );
        
        /**
         * @readonly
         * @function iswindow
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'iswindow' , function ( _ ) {
            return exist( _ ) && ( _ === global || ( exist( _.window ) &&
                exist( _.window.constructor ) && _ instanceof _.window.constructor ) );
        } );
        
        /**
         * @readonly
         * @function getdocument
         * @param {*} _
         * @return {?HTMLDocument}
         * */
        _defineProperty( global , 'getdocument' , function ( _ ) {
            var tmp;
            
            if ( exist( _ ) ) {
                
                if ( isdocument( _ ) ) {
                    return _;
                }
                
                if ( isdocument( tmp = _.ownerDocument ) ) {
                    return tmp;
                }
                
                if ( iswindow( _ ) ) {
                    return _.document;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function getwindow
         * @param {*} _
         * @return {!Window}
         * */
        _defineProperty( global , 'getwindow' , function ( _ ) {
            var tmp;
            
            if ( iswindow( _ ) ) {
                return _;
            }
            
            if ( tmp = getdocument( _ ) ) {
                return tmp.defaultView;
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function isnode
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnode' , function ( _ ) {
            var doc , tag;
            
            if ( ( doc = getdocument( _ ) ) && isstring( tag = _.tagName ) ) {
                return ( doc.createElement( tag ) instanceof _.constructor ||
                    getdocument( global ).createElement( tag ) instanceof _.constructor );
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function isfragment
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfragment' , function ( _ ) {
            var win;
            
            return ( win = getwindow( _ ) ) &&
                ( _ instanceof win.DocumentFragment || _ instanceof global.DocumentFragment );
        } );
        
        /**
         * @readonly
         * @property {boolean} eventPassiveSupport
         * */
        ( function () {
            var tmp ,
                sup = false ,
                document = getdocument( global );
            
            if ( document ) {
                tmp = document.createDocumentFragment();
                
                var empty = function () {} , setting = {
                    get passive () {
                        sup = true;
                        return false;
                    }
                };
                
                tmp.addEventListener( 'click' , empty , setting );
            }
            
            _defineProperty( global , 'eventPassiveSupport' , sup );
        } )();
        
        /**
         * @readonly
         * @function domLoad
         * @param {function} callback
         * */
        _defineProperty( global , 'domLoad' , ( function () {
            var ready = false ,
                stackfunction = [] ,
                document = getdocument( global );
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                if ( !ready ) {
                    ready = true;
                    
                    stackfunction.forEach( function ( callback ) {
                        callback.call( document );
                    } );
                    
                    stackfunction = null;
                    
                    if ( document ) {
                        document.removeEventListener( 'DOMContentLoaded' , load , true );
                        document.removeEventListener( 'DomContentLoaded' , load , true );
                        document.removeEventListener( 'load' , load , true );
                    }
                    
                    global.removeEventListener( 'load' , load , true );
                }
            }
            
            if ( document ) {
                document.addEventListener( 'DOMContentLoaded' , load , true );
                document.addEventListener( 'DomContentLoaded' , load , true );
                document.addEventListener( 'load' , load , true );
            }
            
            global.addEventListener( 'load' , load , true );
            
            return stack;
        } )() );
        
        /**
         * @readonly
         * @function matchesSelector
         * @param {HTMLElement|Element|Node} e - element
         * @param {string} s - css selector
         * @return {!boolean}
         * */
        _defineProperty( global , 'matchesSelector' , ( function () {
            var document = getdocument( global ) ,
                match = null ,
                tmp;
            
            if ( document ) {
                tmp = document.createElement( 'div' );
                
                match = tmp[ 'matches' ] && 'matches' ||
                    tmp[ 'matchesSelector' ] && 'matchesSelector' ||
                    tmp[ 'webkitMatchesSelector' ] && 'webkitMatchesSelector' ||
                    tmp[ 'mozMatchesSelector' ] && 'mozMatchesSelector' ||
                    tmp[ 'oMatchesSelector' ] && 'oMatchesSelector' ||
                    tmp[ 'msMatchesSelector' ] && 'msMatchesSelector' || null;
            }
            
            return function ( e , s ) {
                if ( !match || !e[ match ] ) {
                    return false;
                }
                
                return e[ match ]( s );
            };
        } )() );
        
        /**
         * @readonly
         * @function nl2br
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'nl2br' , function ( str ) {
            if ( isstring( str ) ) {
                return str
                    .replace( /\n/g , "<br/>" )
                    .replace( /\t/g , "&nbsp;&nbsp;&nbsp;&nbsp;" );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function inarray
         * @param {*} _in
         * @param {Array} array
         * @return {!boolean}
         * */
        _defineProperty( global , 'inarray' , function ( _in , array ) {
            return array.indexOf( _in ) >= 0;
        } );
        
        /**
         * @readonly
         * @function arrayunique
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'arrayunique' , function ( _ ) {
            for ( var n = [] , i = 0 , l = _.length ; i < l ; i++ ) {
                if ( !inarray( _[ i ] , n ) ) {
                    n.push( _[ i ] );
                }
            }
            return n;
        } );
        
        /**
         * @readonly
         * @function typedtoarray
         * @param {*} arg
         * @return {!Array}
         * */
        _defineProperty( global , 'typedtoarray' , function ( arg ) {
            return Array.apply( null , arg );
        } );
        
        /**
         * @readonly
         * @function fragmenttoarray
         * @param {DocumentFragment} frag
         * @return {!Array}
         * */
        _defineProperty( global , 'fragmenttoarray' , function ( frag ) {
            return typedtoarray( frag.children );
        } );
        
        /**
         * @readonly
         * @function getallchildren
         * @param {HTMLElement|Element|Node} element
         * @return {!NodeList}
         * */
        _defineProperty( global , 'getallchildren' , function ( element ) {
            return element.getElementsByTagName( '*' );
        } );
        
        /**
         * @readonly
         * @function emptyNode
         * @param {HTMLElement|Element|Node} element
         * @return {!(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'emptyNode' , function ( element ) {
            var c;
            
            while ( c = element.firstChild ) {
                element.removeChild( c );
            }
            
            return element;
        } );
        
        /**
         * @readonly
         * @function closest
         * @param {HTMLElement|Element|Node} element
         * @param {string} selector
         * @return {?(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'closest' , ( function () {
            var document = getdocument( global );
            
            if ( document && isfunction( document.createElement( 'div' ).closest ) ) {
                return function ( element , selector ) {
                    if ( !isnode( element ) ) {
                        return null;
                    }
                    
                    return element.closest( selector );
                };
            }
            
            return function ( element , selector ) {
                if ( !isnode( element ) ) {
                    return null;
                }
                
                if ( matchesSelector( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( matchesSelector( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            };
        } )() );
        
        /**
         * @readonly
         * @function randomstring
         * @return {!string}
         * */
        _defineProperty( global , 'randomstring' , function () {
            return '_' + Math.random().toString( 30 ).substring( 2 );
        } );
        
        /**
         * @readonly
         * @function between
         * @param {number} min
         * @param {number} max
         * @return {!number}
         * */
        _defineProperty( global , 'between' , function ( min , max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) + min );
        } );
        
        /**
         * @readonly
         * @function round
         * @param {number} int
         * @param {number} after
         * @return {!number}
         * */
        _defineProperty( global , 'round' , function ( int , after ) {
            return parseFloat( int.toFixed( isinteger( after ) ? after : int.toString().length ) );
        } );
        
        /**
         * @readonly
         * @function wrapint
         * @param {number} int
         * @param {number} howmany
         * @return {!number}
         * */
        _defineProperty( global , 'wrapinteger' , function ( int , howmany ) {
            int = int.toString();
            
            while ( int.length < howmany ) {
                int = '0' + int;
            }
            
            return int;
        } );
        
        /**
         * @readonly
         * @function jsonparse
         * @param {string} str
         * @return {?Object}
         * */
        _defineProperty( global , 'jsonparse' , function ( str ) {
            var result = null ,
                masterKey;
            
            try {
                result = JSON.parse( str );
            } catch ( e ) {
                try {
                    masterKey = randomstring();
                    
                    global[ masterKey ] = null;
                    
                    eval( 'global[ masterKey ] = ' + str );
                    
                    if ( isobject( global[ masterKey ] ) ) {
                        result = global[ masterKey ] || null;
                    }
                } catch ( e ) {
                    result = null;
                } finally {
                    delete global[ masterKey ];
                }
            }
            
            return result;
        } );
        
        if ( !isfunction( global.setImmediate ) ) {
            ( function () {
                var message = 0 ,
                    register = {} ,
                    prefix = randomstring() + '.immediate';
                
                function getLocation ( location ) {
                    try {
                        if ( isfillstring( location.origin ) && location.origin !== 'null' ) {
                            return location.origin;
                        }
                        
                        if ( location.ancestorOrigins && location.ancestorOrigins.length ) {
                            return location.ancestorOrigins[ 0 ];
                        }
                    } catch ( _ ) {
                        return null;
                    }
                }
                
                var origin = ( function () {
                    var init = global;
                    
                    while ( true ) {
                        var tmp = getLocation( init.location );
                        
                        if ( tmp ) {
                            return tmp;
                        }
                        
                        if ( iswindow( init.parent ) ) {
                            init = init.parent;
                            continue;
                        }
                        
                        return '';
                    }
                } )();
                
                try {
                    global.postMessage( prefix + 1 , function () {} );
                } catch ( e ) {
                    return ( function () {
                        _defineProperty( global , 'setImmediate' , global.setTimeout );
                        _defineProperty( global , 'clearImmediate' , global.clearTimeout );
                    } )();
                }
                
                global.addEventListener( 'message' , function ( event ) {
                    if ( origin === event.origin ) {
                        var i = event.data;
                        
                        if ( isfunction( register[ i ] ) ) {
                            register[ i ]();
                            delete register[ i ];
                            event.stopImmediatePropagation();
                        }
                    }
                } , eventPassiveSupport ? { capture : true , passive : true } : true );
                
                /**
                 * @readonly
                 * @function setImmediate
                 * @param {function} fn
                 * @return {!number}
                 * */
                _defineProperty( global , 'setImmediate' , function ( fn ) {
                    var id = ++message;
                    
                    register[ prefix + id ] = fn;
                    global.postMessage( prefix + id , origin );
                    
                    return id;
                } );
                
                /**
                 * @readonly
                 * @function clearImmediate
                 * @param {number} id
                 * @return void
                 * */
                _defineProperty( global , 'clearImmediate' , function ( id ) {
                    if ( register[ prefix + id ] ) {
                        delete register[ prefix + id ];
                    }
                } );
            } )();
        }
        
        /**
         * @readonly
         * @function getstyleproperty
         * @param {HTMLElement|Element|Node} element
         * @param {string} property
         * @return {?string}
         * */
        _defineProperty( global , 'getstyleproperty' , function ( element , property ) {
            var val , win;
            
            if ( isnode( element ) ) {
                
                if ( val = element.style[ property ] ) {
                    return val;
                }
                
                if ( val = element.style.getPropertyValue( property ) ) {
                    return val;
                }
                
                if ( win = getwindow( element ) ) {
                    return win.getComputedStyle( element ).getPropertyValue( property ) || null;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function splitTrim
         * @param {string} str
         * @param {RegExp|string} spl
         * @return {Array}
         * */
        _defineProperty( global , 'splitTrim' , function ( str , spl ) {
            var ar = str.split( spl );
            
            for ( var i = 0 , r = [] , tmp ; i < ar.length ; ) {
                if ( tmp = ar[ i++ ].trim() ) {
                    r.push( tmp );
                }
            }
            
            return r;
        } );
        
        /**
         * @readonly
         * @function clonearray
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'clonearray' , function ( _ ) {
            return _.slice( 0 );
        } );
        
        /**
         * @readonly
         * @function toarray
         * @param {*} _
         * @return {!Array}
         * */
        _defineProperty( global , 'toarray' , function ( _ ) {
            return isset( _ ) ? ( isarray( _ ) ? _ : [ _ ] ) : [];
        } );
        
        /**
         * @readonly
         * @function counterCallback
         * @param {number} counter
         * @param {function} callback
         * @return {!Array}
         * */
        _defineProperty( global , 'counterCallback' , function ( counter , callback ) {
            var current = 0;
            
            return function () {
                if ( ++current == counter ) {
                    callback();
                }
            };
        } );
        
        /**
         * @readonly
         * @function regexpEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'regexpEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /\\/gi , '\\\\' )
                    .replace( /([.\[\](){}^?*|+\-$])/gi , '\\$1' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function htmlEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'htmlEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /&/gi , '&amp;' )
                    .replace( /"/gi , '&quot;' )
                    .replace( /'/gi , '&apos;' )
                    .replace( /</gi , '&lt;' )
                    .replace( />/gi , '&gt;' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function resetregexp
         * @param {RegExp} _
         * @return {?RegExp}
         * */
        _defineProperty( global , 'resetregexp' , function ( _ ) {
            if ( !isregexp( _ ) ) {
                return null;
            }
            
            return _.lastIndex = _.index = 0, _;
        } );
        
        /**
         * @readonly
         * @function overrideObject
         * @return {!Object}
         * */
        _defineProperty( global , 'overrideObject' , function () {
            var result = {} ,
                array = typedtoarray( arguments );
            
            if ( array.length <= 0 ) {
                return result;
            }
            
            if ( array.length == 1 ) {
                return isobject( array[ 0 ] ) ? array[ 0 ] : result;
            }
            
            for ( var i = 0 , l = array.length ; i < l ; i++ ) {
                forin( array[ i ] , function ( key , value ) {
                    result[ key ] = value;
                } );
            }
            
            return result;
        } );
        
        /**
         * @readonly
         * @function isEmptyObject
         * @param {Object} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isEmptyObject' , function ( _ ) {
            if ( isobject( _ ) ) {
                for ( var key in _ ) {
                    if ( hasProperty.call( _ , key ) ) {
                        return false;
                    }
                }
                
                return true;
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function forin
         * @param {Object} obj
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'forin' , function ( obj , callback ) {
            var key , value;
            
            if ( isobject( obj ) ) {
                for ( key in obj ) {
                    if ( hasProperty.call( obj , key ) && isset( value = obj[ key ] ) ) {
                        callback( key , value , obj );
                    }
                }
            }
        } );
        
        /**
         * @readonly
         * @function foreach
         * @param {Array} arr
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'foreach' , function ( arr , callback ) {
            if ( isarray( arr ) ) {
                for ( var i = 0 , l = arr.length ; i < l ; i++ ) {
                    callback( arr[ i ] , i , arr );
                }
            }
        } );
        
        /**
         * @readonly
         * @function for
         * @param {Array} arg
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'for' , function ( arg , callback ) {
            if ( isarray( arg ) ) {
                foreach( arg , callback );
            }
            else if ( isobject( arg ) ) {
                forin( arg , callback );
            }
        } );
        
        /**
         * @readonly
         * @function smoothyIncrement
         * @param {Object} setting
         * @param {number} setting.begin
         * @param {number} setting.end
         * @param {function} setting.eachFrame
         * @param {function} [setting.onFinish]
         * @param {number} [setting.speed=500]
         * @return void
         * */
        _defineProperty( global , 'smoothyIncrement' , ( function () {
            
            var frameLatency = 1000 / 60;
            
            var requestFrame = global[ 'requestAnimationFrame' ] ||
                global[ 'webkitRequestAnimationFrame' ] ||
                global[ 'mozRequestAnimationFrame' ] ||
                global[ 'msRequestAnimationFrame' ];
            
            var cancelFrame = global[ 'cancelAnimationFrame' ] ||
                global[ 'webkitCancelAnimationFrame' ] ||
                global[ 'mozCancelAnimationFrame' ] ||
                global[ 'msCancelAnimationFrame' ];
            
            function ease ( n ) {
                return 0.5 * ( 1 - Math.cos( Math.PI * n ) );
            }
            
            function animate ( callframe ) {
                var frame , start;
                
                function loop () {
                    frame = requestFrame( loop );
                    callframe();
                }
                
                start = setImmediate( loop );
                
                return function () {
                    clearImmediate( start );
                    cancelFrame( frame );
                };
            }
            
            return function ( setting ) {
                
                setting = overrideObject( {
                    speed : 1000
                } , setting );
                
                var begin = setting.begin ,
                    end = setting.end ,
                    speed = setting.speed ,
                    callback = setting.eachFrame ,
                    callbackFinish = setting.onFinish;
                
                if ( !isinteger( speed ) || speed <= 0 ) {
                    speed = 1000;
                }
                
                speed = Math.round( speed / 1.8 );
                
                var tmp = begin;
                begin = Math.min( begin , end );
                end = Math.max( tmp , end );
                
                var newval , elapsed;
                
                var firstStep = true ,
                    lastStep = false ,
                    endAtNext = false;
                
                var beginValues = [] ,
                    beginTotal = 0 ,
                    total = 0;
                
                var elapsedMax = 1 ,
                    elapsedMin = 0.1;
                
                var totalScroll = end - begin ,
                    increment = Math.round( totalScroll / ( speed / frameLatency ) );
                
                if ( totalScroll <= increment ) {
                    return callback( end , end - begin );
                }
                
                var startTime = Date.now();
                
                var stop = animate( function () {
                    
                    /* stop process */
                    /* ---------------------- */
                    if ( endAtNext ) {
                        callback( end , end - begin );
                        
                        if ( isfunction( callbackFinish ) ) {
                            callbackFinish();
                        }
                        
                        return stop();
                    }
                    
                    /* calc process */
                    /* ---------------------- */
                    
                    elapsed = round( ( Date.now() - startTime ) / speed , 2 );
                    
                    if ( elapsed < elapsedMin ) {
                        elapsed = elapsedMin;
                    }
                    else if ( elapsed > elapsedMax ) {
                        elapsed = elapsedMax;
                    }
                    
                    if ( firstStep && elapsed == elapsedMax ) {
                        firstStep = false;
                    }
                    
                    /* begin process */
                    /* ---------------------- */
                    
                    newval = Math.round( increment * ease( elapsed ) );
                    
                    if ( newval < 1 ) {
                        newval = 1;
                    }
                    
                    /* middle process */
                    /* ---------------------- */
                    
                    if ( firstStep ) {
                        beginValues.push( newval );
                        beginTotal += newval;
                    }
                    
                    if ( !firstStep && !lastStep && ( totalScroll - total ) <= beginTotal ) {
                        lastStep = true;
                    }
                    
                    /* end process */
                    /* ---------------------- */
                    
                    if ( lastStep ) {
                        if ( beginValues.length ) {
                            newval = beginValues.pop();
                        }
                        else {
                            newval = 1;
                        }
                    }
                    
                    total += newval;
                    
                    /* request stop process */
                    /* ---------------------- */
                    
                    if ( lastStep && totalScroll - total <= newval ) {
                        endAtNext = true;
                    }
                    
                    /* callback */
                    /* ---------------------- */
                    
                    callback( begin += newval , newval );
                    
                } );
                
                return stop;
                
            };
            
        } )() );
        
        /**
         * @readonly
         * @function cacheHandler
         * @param {number} [n=500]
         * @return {!function}
         * */
        _defineProperty( global , 'cacheHandler' , function ( n ) {
            var c = [];
            !n && ( n = 500 );
            
            function SAVE ( k , v ) {
                if ( !/^_(rm|purge)$/g.test( k ) ) {
                    SAVE[ k ] = v;
                    c.push( k );
                    
                    if ( c.length > n ) {
                        delete SAVE[ c.shift() ];
                    }
                }
            }
            
            SAVE._rm = function ( k ) {
                var i;
                
                if ( ( i = c.indexOf( k ), i ) !== -1 ) {
                    delete SAVE[ k ];
                    c.splice( i , 1 );
                }
            };
            
            SAVE._purge = function () {
                var i = c.length;
                
                while ( i-- ) {
                    delete SAVE[ c.shift() ];
                }
            };
            
            return SAVE;
        } );
        
        /**
         * @readonly
         * @function requireJS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireJS' , ( function () {
            var doc = getdocument( global );
            
            function require ( src , callback , document ) {
                var script;
                
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && script.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'script[src="' + src + '"]' ) ) {
                        return load();
                    }
                    
                    script = document.createElement( 'script' );
                    
                    script.setAttribute( 'src' , src );
                    script.setAttribute( 'async' , 'async' );
                    script.setAttribute( 'type' , 'text/javascript' );
                    
                    script.addEventListener( 'load' , load );
                    
                    document.head.appendChild( script );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function requireCSS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireCSS' , ( function () {
            var doc = getdocument( global );
            
            function require ( href , callback , document ) {
                var link;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && link.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'link[href="' + href + '"]' ) ) {
                        return load();
                    }
                    
                    link = document.createElement( 'link' );
                    
                    link.setAttribute( 'href' , href );
                    link.setAttribute( 'rel' , 'stylesheet' );
                    
                    link.addEventListener( 'load' , load );
                    
                    document.head.appendChild( link );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function htmlDOM
         * @param {string} str
         * @return {?(HTMLElement|Element|Node|array)}
         * */
        _defineProperty( global , 'htmlDOM' , ( function () {
            var document = getdocument( global );
            
            if ( !document ) {
                return function () {
                    return null;
                };
            }
            
            var parentOf = {
                "col" : "colgroup" ,
                "tr" : "tbody" ,
                "th" : "tr" ,
                "td" : "tr" ,
                "colgroup" : "table" ,
                "tbody" : "table" ,
                "thead" : "table" ,
                "tfoot" : "table" ,
                "dt" : "dl" ,
                "dd" : "dl" ,
                "figcaption" : "figure" ,
                "legend" : "fieldset" ,
                "fieldset" : "form" ,
                "keygen" : "form" ,
                "area" : "map" ,
                "menuitem" : "menu" ,
                "li" : "ul" ,
                "option" : "optgroup" ,
                "optgroup" : "select" ,
                "output" : "form" ,
                "rt" : "ruby" ,
                "rp" : "ruby" ,
                "summary" : "details" ,
                "track" : "video" ,
                "source" : "video" ,
                "param" : "object"
            };
            
            var _ = cacheHandler();
            
            function clone ( e ) {
                if ( isarray( e ) ) {
                    for ( var i = 0 , l = e.length , r = [] ; i < l ; i++ ) {
                        r.push( e[ i ].cloneNode( true ) );
                    }
                    return r;
                }
                
                if ( exist( e ) ) {
                    return e.cloneNode( true );
                }
                
                return null;
            }
            
            function child ( e ) {
                return e.children.length == 1 ? e.children[ 0 ] : Array.apply( null , e.children );
            }
            
            function createHTML ( str ) {
                var firstTag = ( /^(<[\s]*[\w]{1,15}[\s]*(\b|>)?)/gi.exec( str ) || [ '' ] )[ 0 ]
                    .replace( /\W/g , '' ).trim().toLowerCase() ,
                    parent , doc;
                
                if ( !firstTag ) {
                    return;
                }
                
                if ( !parentOf[ firstTag ] ) {
                    doc = document.createDocumentFragment();
                    parent = document.createElement( 'DIV' );
                    doc.appendChild( parent );
                    parent.innerHTML = str;
                    return child( parent );
                }
                
                /** @type {Node} */
                parent = createHTML( '<' + parentOf[ firstTag ] + '></' + parentOf[ firstTag ] + '>' );
                
                while ( parent.firstChild ) {
                    parent = parent.firstChild;
                }
                
                parent.innerHTML = str;
                
                return child( parent );
            }
            
            return function ( str ) {
                var r;
                
                str = str.trim();
                
                if ( _[ str ] ) {
                    return clone( _[ str ] );
                }
                
                if ( !/(<[\s]*(\/)?[\s]*[\w]{1,15}[\s]*(\/)?[\s]*(\b|>)?)/gi.test( str ) ) {
                    return null;
                }
                
                if ( /^(<\/[\s]*[\w]{1,15}[\s]*>)$/gi.test( str ) ) {
                    r = document.createElement( str.replace( /\W/g , '' ).toUpperCase() );
                }
                else {
                    r = createHTML( str );
                }
                
                return _( str , r ) , clone( r );
            };
        } )() );
        
    } )();
    
    function encodeValue ( value ) {
        if ( exist( value ) ) {
            return '=' + encodeURIComponent( value );
        }
        
        return '';
    }
    
    function formatFormData ( data ) {
        var r = [];
        
        forin( data , function ( key , value ) {
            r.push( encodeURIComponent( key ) + encodeValue( value ) );
        } );
        
        return r.join( '&' );
    }
    
    function formatQueryString ( url , queries ) {
        var r = [];
        
        forin( queries , function ( key , value ) {
            r.push( encodeURIComponent( key ) + encodeValue( value ) );
        } );
        
        if ( r.length ) {
            url += ( url.indexOf( '?' ) !== -1 ? '&' : '?' ) + r.join( '&' );
        }
        
        return url;
    }
    
    function parseArguments ( url , callback , proxy ) {
        var tmp;
        
        if ( isobject( url ) ) {
            return url;
        }
        
        tmp = { url : url };
        
        if ( isfunction( callback ) ) {
            tmp.anyway = function ( x ) {
                proxy( callback , x );
            };
        }
        
        return tmp;
    }
    
    function toDocumentFragment ( HTMLDocument ) {
        var frag , children;
        
        if ( !exist( HTMLDocument ) ||
            !( children = HTMLDocument.body.children ).length ) {
            return null;
        }
        
        frag = document.createDocumentFragment();
        
        while ( children.length ) {
            frag.appendChild( children[ 0 ] );
        }
        
        return frag;
    }
    
    /**
     * @readonly
     * @function ajax
     * @param {Object} setting
     *
     * @param {*} [setting.brut]
     * @param {Object} [setting.get]
     * @param {Object} [setting.post]
     *
     * @param {string} setting.url
     * @param {boolean} [setting.async=true]
     * @param {boolean} [setting.cache=true]
     * @param {boolean} [setting.crossDomain=false]
     * @param {boolean} [setting.withCredentials=false]
     *
     * @param {string} [setting.type=GET]
     * @param {Object} [setting.headers]
     * @param {number} [setting.timeoutDelay=90 seconds]
     *
     * @param {string} [setting.mimeType]
     * @param {string} [setting.responseType=text]
     *
     * @param {function} [setting.done]
     * @param {function} [setting.then]
     *
     * @param {function} [setting.error]
     * @param {function} [setting.fail]
     *
     * @param {function} [setting.abort]
     * @param {function} [setting.aborted]
     *
     * @param {function} [setting.timeout]
     * @param {function} [setting.timeup]
     *
     * @param {function} [setting.anyway]
     * @param {function} [setting.finally]
     *
     * @return {!Object}
     * */
    _defineProperty( global , 'ajax' , function ( setting ) {
        var x = new XMLHttpRequest() ,
            index = ( ++ajax.idx ) ,
            defaultHeaders = {} ,
            formData = null ,
            timeout;
        
        // default
        // ---------------------------
        setting = overrideObject( {
            url : null ,
            
            get : null ,
            post : null ,
            brut : null ,
            
            async : true ,
            cache : true ,
            crossDomain : false ,
            withCredentials : false ,
            
            type : 'GET' ,
            headers : {} ,
            timeoutDelay : 90 ,
            
            mimeType : null ,
            responseType : 'text'
        } , setting );
        
        if ( !isstring( setting.url ) ) {
            throw 'ajax error : setting.url argument cannot be empty.';
        }
        
        // callbacks
        // ---------------------------
        var calltypes = [];
        
        var calltypesAlias = {
            'done' : [ 'then' ] ,
            'error' : [ 'fail' ] ,
            'abort' : [ 'aborted' ] ,
            'timeout' : [ 'timeup' ] ,
            'anyway' : [ 'finally' ]
        };
        
        forin( calltypesAlias , function ( type , alias ) {
            calltypes.push( type );
            Array.prototype.push.apply( calltypes , alias );
        } );
        
        function getCallbackType ( key ) {
            if ( isfillstring( key ) ) {
                
                if ( calltypesAlias[ key ] ) {
                    return key;
                }
                
                for ( var type in calltypesAlias ) {
                    if ( inarray( key , calltypesAlias[ type ] ) ) {
                        return type;
                    }
                }
                
            }
            
            return null;
        }
        
        var callbacks = {
            
            stack : function ( type , callback ) {
                type = getCallbackType( type );
                
                if ( type && isfunction( callback ) ) {
                    
                    if ( !callbacks[ type ] ) {
                        callbacks[ type ] = [];
                    }
                    
                    if ( setting.override && setting.override[ type ] ) {
                        
                        return callbacks[ type ].push( ( function ( bypass ) {
                            
                            return function ( x ) {
                                bypass.call( x , x , callback );
                            };
                            
                        } )( setting.override[ type ] ) );
                        
                    }
                    
                    callbacks[ type ].push( callback );
                    
                }
            } ,
            
            unstack : function ( type , xhr ) {
                type = getCallbackType( type );
                
                if ( callbacks[ type ] && callbacks[ type ].length ) {
                    
                    callbacks[ type ].forEach( function ( callback ) {
                        callback.call( xhr , xhr );
                    } );
                    
                    callbacks[ type ] = [];
                    
                    return true;
                    
                }
                
                return false;
            }
            
        };
        
        calltypes.forEach( function ( type ) {
            callbacks.stack( type , setting[ type ] );
        } );
        
        // settings
        // ---------------------------
        
        // http request
        setting.type = setting.type.toUpperCase();
        
        // with credentials
        if ( isboolean( setting.withCredentials ) ) {
            x.withCredentials = setting.withCredentials;
        }
        
        // format get arguments
        if ( isobject( setting.get ) ) {
            setting.url = formatQueryString( setting.url , setting.get );
        }
        
        // format post arguments
        if ( isobject( setting.post ) ) {
            formData = formatFormData( setting.post );
            
            if ( /^(GET|HEAD|TRACE|OPTIONS|CONNECT)$/.test( setting.type ) ) {
                setting.type = 'POST';
            }
        }
        
        // set brut data
        if ( exist( setting.brut ) ) {
            formData = setting.brut;
            
            if ( setting.type !== 'BRUT' ) {
                setting.type = 'BRUT';
            }
        }
        
        // set responseType
        if ( isstring( setting.responseType ) ) {
            try {
                x.responseType = setting.responseType;
            } catch ( _ ) {
                x.responseType = 'text';
            }
        }
        
        // mimetype
        if ( isstring( setting.mimeType ) ) {
            x.overrideMimeType( setting.mimeType );
        }
        
        // request destructor
        // ---------------------------
        function destroy () {
            if ( index && isfunction( ajax.request[ index ] ) ) {
                ajax.request[ index ]();
            }
        }
        
        x.aborted = false;
        
        ajax.request[ index ] = function () {
            if ( !x || !index ) {
                return;
            }
            
            delete ajax.request[ index ];
            clearTimeout( timeout );
            
            if ( !x.aborted && x.readyState < 4 ) {
                x.aborted = true;
                x.abort();
            }
            
            ajax.active--;
            index = null;
        };
        
        // abort
        // ---------------------------
        x.addEventListener( 'abort' , function () {
            destroy();
            
            callbacks.unstack( 'abort' , x );
            callbacks.unstack( 'anyway' , x );
        } , { once : true } );
        
        // error
        // ---------------------------
        x.addEventListener( 'error' , function () {
            destroy();
            
            callbacks.unstack( 'error' , x );
            callbacks.unstack( 'anyway' , x );
        } , { once : true } );
        
        // timeout
        // ---------------------------
        x.addEventListener( 'timeout' , function () {
            destroy();
            
            if ( !callbacks.unstack( 'timeout' , x ) ) {
                callbacks.unstack( 'error' , x );
            }
            
            callbacks.unstack( 'anyway' , x );
        } , { once : true } );
        
        if ( isinteger( setting.timeoutDelay ) && setting.timeoutDelay >= 0 ) {
            x.timeout = setting.timeoutDelay * 1000;
        }
        
        // response
        // ---------------------------
        x.addEventListener( 'load' , function () {
            destroy();
            
            // server / request error
            if ( /^[045]/g.test( x.status + '' ) ) {
                callbacks.unstack( 'error' , x );
            }
            // all good
            else {
                callbacks.unstack( 'done' , x );
            }
            
            callbacks.unstack( 'anyway' , x );
        } , { once : true } );
        
        // create request
        // ---------------------------
        switch ( setting.type ) {
            
            case 'BRUT':
                ( function () {
                    defaultHeaders[ 'Content-Type' ] = 'multipart/form-data';
                    x.open( 'POST' , setting.url , setting.async );
                } )();
                break;
            
            case 'PUT':
            case 'POST':
            case 'PATCH':
            case 'DELETE':
            case 'UPDATE':
                ( function () {
                    defaultHeaders[ 'Content-Type' ] = 'application/x-www-form-urlencoded';
                    x.open( setting.type , setting.url , setting.async );
                } )();
                break;
            
            case 'GET':
            case 'HEAD':
            case 'TRACE':
            case 'OPTIONS':
            case 'CONNECT':
                ( function () {
                    // disable cache
                    if ( !setting.cache ) {
                        defaultHeaders[ 'Cache-Control' ] = 'no-cache, no-store, must-revalidate';
                        defaultHeaders[ 'Pragma' ] = 'no-cache';
                        defaultHeaders[ 'Expires' ] = '0';
                        
                        setting.url = formatQueryString( setting.url , {
                            '__' : Date.now()
                        } );
                    }
                    
                    x.open( setting.type , setting.url , setting.async );
                } )();
                break;
            
        }
        
        // cross domain request
        // ---------------------------
        if ( setting.crossDomain ) {
            defaultHeaders[ 'Accept' ] = '*/*';
        }
        else {
            defaultHeaders[ 'X-Requested-With' ] = 'XMLHttpRequest';
        }
        
        // headers
        // ---------------------------
        forin( overrideObject( defaultHeaders , setting.headers ) , function ( key , value ) {
            x.setRequestHeader( key , value );
        } );
        
        // send
        // ---------------------------
        ajax.active++;
        x.send( formData );
        
        // actions
        // ---------------------------
        var result = {
            xhr : x ,
            setting : setting
        };
        
        calltypes.forEach( function ( type ) {
            result[ type ] = function ( callback ) {
                callbacks.stack( type , callback );
                return result;
            };
        } );
        
        result.abort = function ( callback ) {
            if ( isfunction( callback ) ) {
                callbacks.stack( 'abort' , callback );
                return result;
            }
            
            destroy();
        };
        
        return result;
    } );
    
    /**
     * @private
     * @property {Object} ajax.request
     * */
    _defineProperty( ajax , 'request' , {} );
    
    /**
     * @property {number} ajax.active
     * */
    ajax.active = 0;
    
    /**
     * @private
     * @property {number} ajax.idx
     * */
    ajax.idx = 0;
    
    /**
     * @readonly
     * @function getText
     * @param {string|Object} url
     * @param {function} [callback]
     * @return {!Object}
     * */
    _defineProperty( global , 'getText' , function ( url , callback ) {
        return ajax( overrideObject(
            parseArguments(
                // user settings
                url , callback ,
                
                // anyway
                function ( callback , x ) {
                    callback.call( x , x.response || '' , x );
                }
            ) ,
            
            // default settings
            {
                responseType : 'text' ,
                override : {
                    done : function ( x , origin ) {
                        origin.call( x , x.response || '' , x );
                    } ,
                    error : function ( x , origin ) {
                        origin.call( x , x.response || '' , x );
                    }
                }
            }
        ) );
    } );
    
    /**
     * @readonly
     * @function getJson
     * @param {string|Object} url
     * @param {function} [callback]
     * @return {!Object}
     * */
    _defineProperty( global , 'getJson' , function ( url , callback ) {
        return ajax( overrideObject(
            parseArguments(
                // user settings
                url , callback ,
                
                // anyway
                function ( callback , x ) {
                    callback.call( x , x.response || null , x );
                }
            ) ,
            
            // default settings
            {
                responseType : 'json' ,
                override : {
                    done : function ( x , origin ) {
                        origin.call( x , x.response || null , x );
                    } ,
                    error : function ( x , origin ) {
                        origin.call( x , x.response || null , x );
                    }
                }
            }
        ) );
    } );
    
    /**
     * @readonly
     * @function getXML
     * @param {string|Object} url
     * @param {function} [callback]
     * @return {!Object}
     * */
    _defineProperty( global , 'getXML' , function ( url , callback ) {
        return ajax( overrideObject(
            parseArguments(
                // user settings
                url , callback ,
                
                // anyway
                function ( callback , x ) {
                    callback.call( x , x.response || null , x );
                }
            ) ,
            
            // default settings
            {
                mimeType : 'text/xml' ,
                responseType : 'document' ,
                override : {
                    done : function ( x , origin ) {
                        origin.call( x , x.response || null , x );
                    } ,
                    error : function ( x , origin ) {
                        origin.call( x , x.response || null , x );
                    }
                }
            }
        ) );
    } );
    
    /**
     * @readonly
     * @function getHTML
     * @param {string|Object} url
     * @param {function} [callback]
     * @return {!Object}
     * */
    _defineProperty( global , 'getHTML' , function ( url , callback ) {
        return ajax( overrideObject(
            parseArguments(
                // user settings
                url , callback ,
                
                // anyway
                function ( callback , x ) {
                    callback.call( x , toDocumentFragment( x.response ) , x );
                }
            ) ,
            
            // default settings
            {
                mimeType : 'text/html' ,
                responseType : 'document' ,
                override : {
                    done : function ( x , origin ) {
                        origin.call( x , toDocumentFragment( x.response ) , x );
                    } ,
                    error : function ( x , origin ) {
                        origin.call( x , toDocumentFragment( x.response ) , x );
                    }
                }
            }
        ) );
    } );
    
    /**
     * @readonly
     * @function getFile
     * @param {string|Object} url
     * @param {function} [callback]
     * @return {!Object}
     * */
    _defineProperty( global , 'getFile' , function ( url , callback ) {
        return ajax( overrideObject(
            parseArguments(
                // user settings
                url , callback ,
                
                // anyway
                function ( callback , x ) {
                    callback.call( x , x.response || null , x );
                }
            ) ,
            
            // default settings
            {
                responseType : 'blob' ,
                override : {
                    done : function ( x , origin ) {
                        origin.call( x , x.response || null , x );
                    } ,
                    error : function ( x , origin ) {
                        origin.call( x , x.response || null , x );
                    }
                }
            }
        ) );
    } );
    
} )();